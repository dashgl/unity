﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CharacterConfigurator
{
    [Serializable]
    public class TextureList
    {
        public List<int> textures;

        public TextureList()
        {
            textures = new List<int>();
        }

        public void Add(int value)
        {
            textures.Add(value);
        }
    }

    [Serializable]
    public class ModelDefinitions
    {
        [SerializeField]
        List<int> _capModels = new List<int>();
        [SerializeField]
        List<int> _hairModels = new List<int>();
        [SerializeField]
        List<TextureList> _capTextures = new List<TextureList>();
        [SerializeField]
        List<TextureList> _hairTextures = new List<TextureList>();
        [SerializeField]
        TextureList _bodyTextures = new TextureList();

        [SerializeField]
        int _costumeOffset;

        [SerializeField]
        public string _class = "ramar";

        [SerializeField]
        //        public string _modelPrefix = "plG";     // fomarl
        public string _modelPrefix = "plD";     // ramar

        public static ModelDefinitions CreateFromJSON(string filePath)
        {
            string jsonString = File.ReadAllText(filePath);

            return JsonUtility.FromJson<ModelDefinitions>(jsonString);
        }

        public bool HasBodyTextures()
        {
            if (_bodyTextures.textures.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetBodyTexture(int matIndex)
        {
            return _bodyTextures.textures[matIndex];
        }

        public int GetCapTexture(int capIndex, int matIndex)
        {
            return _capTextures[capIndex].textures[matIndex];
        }

        public int GetHairTexture(int hairIndex, int matIndex)
        {
            return _hairTextures[hairIndex].textures[matIndex];
        }

        public int GetCostumeOffset()
        {
            return _costumeOffset;
        }

        public bool HasCapModels()
        {
            if (_capModels.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetCapModelIndex(int variation)
        {
            return _capModels[variation];
        }

        public bool HasHairModels()
        {
            if (_hairModels.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetHairModelIndex(int variation)
        {
            return _hairModels[variation];
        }
    }

    ModelDefinitions _modelDefinitions;

    GameObject _body;
    GameObject _hair;
    GameObject _cap;

    public int _costume = 1;
    public int _capVariation = 0;
    public int _hairVariation = 0;
//    public Color _hairColor = new Color(0.35f, 0.5f, 0.92f);
    public Color _hairColor = new Color(1.0f, 0.7f, 0.3f);

    public void Init(string characterClass)
    {
        string path = Application.streamingAssetsPath + "/" + characterClass  + "/" + characterClass + ".json";

        _modelDefinitions = ModelDefinitions.CreateFromJSON(path);
    }

    public void SetModelLinks(GameObject body, GameObject hair, GameObject cap)
    {
        _body = body;
        _hair = hair;
        _cap = cap;
    }

    public void ApplyConfiguration()
    {
        if (_modelDefinitions.HasBodyTextures() == true)
        {
            // apply body textures
            //
            Renderer bodyRenderer = _body.GetComponent<Renderer>();
            for (int materialIndex = 0; materialIndex < bodyRenderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetBodyTexture(materialIndex);
                textureIndex = GetCostumeOffset(textureIndex);
                string texturePath = GetTexturePath(textureIndex);
//                Texture2D texture = Resources.Load<Texture2D>(texturePath);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));
                bodyRenderer.sharedMaterials[materialIndex].mainTexture = texture;
            }
        }

        if (_modelDefinitions.HasCapModels() == true)
        {
            // apply cap textures
            Renderer renderer = _cap.GetComponentInChildren<Renderer>();
            for (int materialIndex = 0; materialIndex < renderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetCapTexture(_capVariation, materialIndex);
                textureIndex = GetCostumeOffset(textureIndex);
                string texturePath = GetTexturePath(textureIndex);
//                Texture2D texture = Resources.Load<Texture2D>(texturePath);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));
                renderer.sharedMaterials[materialIndex].mainTexture = texture;
            }
        }

        if (_modelDefinitions.HasHairModels() == true)
        {
            // apply hair textures and color
            Renderer renderer = _hair.GetComponentInChildren<Renderer>();
            for (int materialIndex = 0; materialIndex < renderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetHairTexture(_hairVariation, materialIndex);
                textureIndex = GetCostumeOffset(textureIndex);
                string texturePath = GetTexturePath(textureIndex);
//                Texture2D texture = Resources.Load<Texture2D>(texturePath);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));
                renderer.sharedMaterials[materialIndex].mainTexture = texture;
                renderer.sharedMaterials[materialIndex].SetColor("_Color", _hairColor);
            }
        }
    }

    private string GetTexturePath(int textureIndex)
    {
        string texturePath = "Assets/Textures/" + _modelDefinitions._class + "/" +
                                                  _modelDefinitions._modelPrefix + "tex_" + string.Format("{0:000}", textureIndex) + ".png";
        return texturePath;
    }

    private int GetCostumeOffset(int textureIndex)
    {
        int costume = (_costume % 9);

        if (textureIndex >= 10000)
        {
            int customCostumeOffset = textureIndex / 10000;
            textureIndex = textureIndex % 10000;
            textureIndex += customCostumeOffset * costume;
        }
        else if (textureIndex < _modelDefinitions.GetCostumeOffset())
        {
            int costumeOffset = costume * _modelDefinitions.GetCostumeOffset();
            textureIndex += costumeOffset;
        }

        return textureIndex;
    }
}
