/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DashGL;

public class MeshCreator
{
    Vector3[] _vertexData;
    Vector3[] _normalData;
    Vector2[] _uv1Data;
    int[] _indexData;
    int[] _materialData;

    public Mesh CreateMesh(DashLoader loader)
    {
        List<Vector3> vertices = loader.GetVertices();
        List<VertexBoneAttributes> boneWeights = loader.GetVertexBoneAttributes();
        List<Vector3> normals = loader.GetNormals();
        List<Color> vertexColors = loader.GetVertexColors();
        int[] indices = loader.GetIndices();
        List<Vector2> uv1s = loader.GetUvs();
        List<int> materials = loader.GetMaterialIds();
        int materialCount = loader.GetMaterialCount();

        Mesh mesh = new Mesh();

        if (indices.Length <= 65535)
        {
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt16;
        }
        else
        {
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        }

        List<Vector3> vertexList = new List<Vector3>();
        List<BoneWeight> boneWeightList = new List<BoneWeight>();
        List<int> indexList = new List<int>();

        for (int count = 0; count < indices.Length; count++)
        {
            vertexList.Add(vertices[indices[count]]);

            // compose unity bone weight
            VertexBoneAttributes boneAttribute = boneWeights[indices[count]];
            BoneWeight weight = new BoneWeight();
            weight.boneIndex0 = (int)boneAttribute.index1;
            weight.weight0 = boneAttribute.weight1;
            weight.boneIndex1 = (int)boneAttribute.index2;
            weight.weight1 = boneAttribute.weight2;
            weight.boneIndex2 = (int)boneAttribute.index3;
            weight.weight2 = boneAttribute.weight3;
            weight.boneIndex3 = (int)boneAttribute.index4;
            weight.weight3 = boneAttribute.weight4;
            boneWeightList.Add(weight);

            indexList.Add(count);
        }
        _indexData = indexList.ToArray();

        _vertexData = vertexList.ToArray();
        _normalData = normals.ToArray();
        _uv1Data = uv1s.ToArray();
        _materialData = materials.ToArray();    // per face materials

        mesh.SetVertices(new List<Vector3>(_vertexData));
        mesh.boneWeights = boneWeightList.ToArray();
        mesh.SetNormals(new List<Vector3>(_normalData));
        //        mesh.SetIndices(_indexData, MeshTopology.Triangles, 0);
        mesh.SetUVs(0, new List<Vector2>(_uv1Data));
        mesh.SetColors(vertexColors);

        // build submesh index lists
        //
        List<List<int>> subMeshes = new List<List<int>>();

        for (int count = 0; count < materialCount; count++)
        {
            subMeshes.Add(new List<int>());
        }

        for (int indexCount = 0; indexCount < _indexData.Length; indexCount += 3)
        {
            //            int material = _materialData[_indexData[indexCount]];
            int material = _materialData[indexCount / 3];

            subMeshes[material].Add(_indexData[indexCount]);
            subMeshes[material].Add(_indexData[indexCount + 1]);
            subMeshes[material].Add(_indexData[indexCount + 2]);
        }

        mesh.subMeshCount = subMeshes.Count;

        for (int subMeshCount = 0; subMeshCount < subMeshes.Count; subMeshCount++)
        {
            mesh.SetIndices(subMeshes[subMeshCount].ToArray(), MeshTopology.Triangles, subMeshCount);
        }

        mesh.name = "mesh";

        NormalSolver.RecalculateNormals(mesh, 90f);

        return mesh;
    }

    public Mesh[] CreateMeshes(DashLoader loader)
    {
        List<Vector3> vertices = loader.GetVertices();
        List<Vector3> normals = loader.GetNormals();
        List<Color> vertexColors = loader.GetVertexColors();
        int[] indices = loader.GetIndices();
        List<Vector2> uv1s = loader.GetUvs();
        List<int> materials = loader.GetMaterialIds();
        int materialCount = loader.GetMaterialCount();

        Mesh[] meshes = new Mesh[materialCount];

        List<Vector3> vertexList = new List<Vector3>();
        List<int> indexList = new List<int>();

        for (int count = 0; count < indices.Length; count++)
        {
            vertexList.Add(vertices[indices[count]]);
            indexList.Add(count);
        }
        _indexData = indexList.ToArray();

        _vertexData = vertexList.ToArray();
        _normalData = normals.ToArray();
        _uv1Data = uv1s.ToArray();
        _materialData = materials.ToArray();    // per face materials

        // build submesh index lists
        //
        List<List<int>> subMeshes = new List<List<int>>();

        for (int count = 0; count < materialCount; count++)
        {
            subMeshes.Add(new List<int>());
        }

        for (int indexCount = 0; indexCount < _indexData.Length; indexCount += 3)
        {
            int material = _materialData[indexCount / 3];

            subMeshes[material].Add(_indexData[indexCount]);
            subMeshes[material].Add(_indexData[indexCount + 1]);
            subMeshes[material].Add(_indexData[indexCount + 2]);
        }

        for (int subMeshCount = 0; subMeshCount < subMeshes.Count; subMeshCount++)
        {
            if (subMeshes[subMeshCount].Count == 0)
            {
                meshes[subMeshCount] = null;
                continue;
            }

            Mesh mesh = new Mesh();
            mesh.name = "mesh" + subMeshCount;

            List<Vector3> purgedVertices = new List<Vector3>();
            List<Vector3> purgedNormals = new List<Vector3>();
            List<Vector2> purgedUvs = new List<Vector2>();
            List<Color> purgedColors = new List<Color>();
            List<int> purgedIndices = new List<int>();

            for (int purgeCount = 0; purgeCount < subMeshes[subMeshCount].Count; purgeCount++)
            {
                int index = subMeshes[subMeshCount][purgeCount];
                purgedVertices.Add(_vertexData[index]);
                purgedNormals.Add(_normalData[index]);
                purgedUvs.Add(_uv1Data[index]);
                purgedColors.Add(vertexColors[index]);

                purgedIndices.Add(purgeCount);
            }

            mesh.SetVertices(purgedVertices);
            mesh.SetNormals(purgedNormals);
            mesh.SetUVs(0, purgedUvs);
            mesh.SetColors(purgedColors);
            mesh.SetIndices(purgedIndices.ToArray(), MeshTopology.Triangles, 0);

            if (purgedIndices.Count <= 65535)
            {
                mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt16;
            }
            else
            {
                mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            }

            NormalSolver.RecalculateNormals(mesh, 90f);
            meshes[subMeshCount] = mesh;
        }

        return meshes;
    }

    private Vector2 GetUv1(int index)
    {
        Vector2 uv = Vector2.zero;

        if (_uv1Data != null)
        {
            if (index < _uv1Data.Length)
            {
                uv = _uv1Data[index];
            }
        }

        return uv;
    }


    private Vector2 GetUv2(int index)
    {
        Vector2 uv = Vector2.zero;
        /*
                if (Uv2Data != null)
                {
                    if (index < Uv2Data.Length)
                    {
                        uv = Uv2Data[index];
                    }
                }
        */
        return uv;
    }

    private int GetMaterial(int index)
    {
        int material = 0;

        if (_materialData != null)
        {
            if (index < _materialData.Length)
            {
                material = _materialData[index];
            }
        }

        return material;
    }
}
