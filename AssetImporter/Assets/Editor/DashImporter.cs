/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/
 
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DashGL;
using System.IO;
using UnityEditor;

namespace DashImporter
{
    public class Importer
    {
        public enum BlendMode
        {
            Opaque,
            Cutout,
            Fade,       // Old school alpha-blending mode, fresnel does not affect amount of transparency
            Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
        }

        public enum CullingMode
        {
            Off,
            Front,
            Back
        }

        private static string _sourcePath;
//        public static NodeData[] _rootNodes;

        private DashLoader _loader;

        public List<Texture2D> _textures = new List<Texture2D>();

        public void Import(string sourcePath, UnityAction<GameObject> finishedAction)
        {
            _loader = new DashLoader();

            // get unity obj. name
            string objectName = FileSystemHelper.GetFileNameWithoutExtensionFromPath(sourcePath);

            // load and parse fbx via thread
            //
            _sourcePath = sourcePath;

            bool successful = _loader.LoadAndParse(_sourcePath);

            if (successful == false)
            {
                return;
            }

            // create textures
            //
            List<Byte[]> textures = _loader.GetTextures();
            List<ImageAttributes> texAttributes = _loader.GetTextureAttributes();

            for (int count = 0; count < textures.Count; count++)
            {
                ImageAttributes attributes = texAttributes[count];

                // write texture file
                //
                string textureFilePath = "Assets/" + objectName + "/Textures/" + attributes.name + ".png";
                FileSystemHelper.EnsureDirectory(textureFilePath);
                File.WriteAllBytes(textureFilePath, textures[count]);

                AssetDatabase.Refresh();
                AssetDatabase.ImportAsset(textureFilePath);

                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(textureFilePath, typeof(UnityEngine.Texture));
                texture.LoadImage(textures[count]);

                switch (attributes.wrapS)
                {
                    case DashConstants.DASH_TEXTURE_WRAP:
                        texture.wrapModeU = TextureWrapMode.Repeat;
                        break;
                    case DashConstants.DASH_TEXTURE_CLAMP:
                        texture.wrapModeU = TextureWrapMode.Clamp;
                        break;
                    case DashConstants.DASH_TEXTURE_MIRROR:
                        texture.wrapModeU = TextureWrapMode.Mirror;
                        break;
                }

                switch (attributes.wrapT)
                {
                    case DashConstants.DASH_TEXTURE_WRAP:
                        texture.wrapModeV = TextureWrapMode.Repeat;
                        break;
                    case DashConstants.DASH_TEXTURE_CLAMP:
                        texture.wrapModeV = TextureWrapMode.Clamp;
                        break;
                    case DashConstants.DASH_TEXTURE_MIRROR:
                        texture.wrapModeV = TextureWrapMode.Mirror;
                        break;
                }

                _textures.Add(texture);

            }

            // import / create meshes
            //
            bool skinnedMesh = true;

            if (_loader.GetBoneAttributes().Count == 0)
            {
                skinnedMesh = false;
            }

            // import / create meshes
            //
            MeshCreator meshCreator = new MeshCreator();

            Mesh[] meshes = null;

            if (skinnedMesh == true)
            {
                meshes = new Mesh[1];
                meshes[0] = meshCreator.CreateMesh(_loader);
            }
            else
            {
                meshes = meshCreator.CreateMeshes(_loader);
            }

            // write mesh assets
            foreach (Mesh mesh in meshes)
            {
                if (mesh != null)
                {
                    string meshFilePath = "Assets/" + objectName + "/Meshes/" + mesh.name + ".asset";
                    FileSystemHelper.EnsureDirectory(meshFilePath);
                    AssetDatabase.CreateAsset(mesh, meshFilePath);
                }
            }

            GameObject go = new GameObject();
//            go.SetActive(false);
            go.transform.localScale = new Vector3(-0.1f, 0.1f, 0.1f);

            Renderer renderer = null;
            List<GameObject> meshObjects = new List<GameObject>();
            List<GameObject> boneObjects = new List<GameObject>();

            if (skinnedMesh == false)
            {
                foreach (Mesh submesh in meshes)
                {
                    if (submesh == null)
                    {
                        meshObjects.Add(null);
                        continue;
                    }

                    GameObject subObject = new GameObject("mat" + meshObjects.Count);
                    subObject.SetActive(true);

                    subObject.transform.parent = go.transform;

                    subObject.transform.localScale = new Vector3(1f, 1f, 1f);

                    MeshFilter filter = subObject.AddComponent<MeshFilter>();
                    filter.mesh = submesh;

                    renderer = subObject.AddComponent<MeshRenderer>();

                    meshObjects.Add(subObject);
                }
            }
            else
            {
                renderer = go.AddComponent<SkinnedMeshRenderer>();
                ((SkinnedMeshRenderer)renderer).sharedMesh = meshes[0];

                // build bones hierarchy
                //
                List<BoneAttributes> bones = _loader.GetBoneAttributes();
                List<Transform> boneTransforms = new List<Transform>();
                List<Matrix4x4> bindPoses = new List<Matrix4x4>();

                foreach (BoneAttributes bone in bones)
                {
                    GameObject boneObject = new GameObject(bone.name);

                    if (bone.parentId == -1)
                    {
                        boneObject.transform.parent = go.transform;
                    }
                    else
                    {
                        boneObject.transform.parent = boneObjects[bone.parentId].transform;
                    }

                    Matrix4x4 matrix = bone.matrix;

                    boneObject.transform.localPosition = MatrixHelper.ExtractTranslationFromMatrix(ref matrix);
                    boneObject.transform.localRotation = MatrixHelper.ExtractRotationFromMatrix(ref matrix);
                    boneObject.transform.localScale = MatrixHelper.ExtractScaleFromMatrix(ref matrix);

                    boneObjects.Add(boneObject);
                    boneTransforms.Add(boneObject.transform);

                    Matrix4x4 bindPose = new Matrix4x4();
                    bindPose = boneObject.transform.worldToLocalMatrix * go.transform.localToWorldMatrix;
                    bindPoses.Add(bindPose);
                }

                meshes[0].bindposes = bindPoses.ToArray();
                ((SkinnedMeshRenderer)renderer).bones = boneTransforms.ToArray();
            }

            // create materials
            //
            List<Material> materials = new List<Material>();
            List<MaterialAttributes> matAttributes = _loader.GetMaterialAttributes();

            for (int count = 0; count < matAttributes.Count; count++)
            {
//                Material material = new Material(Shader.Find("Standard"));
                Material material = new Material(Shader.Find("Standard (Vertex Color)"));
                material.EnableKeyword("_VERTEXCOLOR");
                material.SetFloat("_Culling", (float)CullingMode.Off);

                //                Material material = new Material(Shader.Find("Unlit/Vertex Colour Tester"));
                //                material.SetFloat("_AlphaOn", 1f);

                material.name = "M" + count;

                int textureId = matAttributes[count].textureId;
                if (textureId != -1)
                {
                    material.mainTexture = _textures[textureId];
                    material.name = material.name + "_" + _textures[textureId].name;
                   
                    Vector2 textureScale = Vector2.one;

                    if (texAttributes[textureId].flipY == 0)
                    {
                        textureScale.y = -1f;
                    }
                    material.SetTextureScale("_MainTex", textureScale);

                    ChangeRenderMode(material, BlendMode.Cutout);
                }

                materials.Add(material);

                // write asset
                string materialFilePath = "Assets/" + objectName + "/Materials/" + material.name + ".mat";
                FileSystemHelper.EnsureDirectory(materialFilePath);
                AssetDatabase.CreateAsset(material, materialFilePath);
//                AssetDatabase.ImportAsset(materialFilePath);
            }

            if (skinnedMesh == true)
            {
                renderer.sharedMaterials = materials.ToArray();
            }
            else
            {
                for (int matCount = 0; matCount < materials.Count; matCount++)
                {
                    if (matCount < meshObjects.Count)
                    {
                        if (meshObjects[matCount] != null)
                        {
                            meshObjects[matCount].GetComponent<Renderer>().sharedMaterial = materials[matCount];
                            meshObjects[matCount].name = materials[matCount].name;
                        }
                    }
                }
            }

            go.name = objectName;

            // create animations / clips
            //
            Animation anim = go.AddComponent<Animation>();

            List<AnimationAttributes> animationAttributes = _loader.GetAnimationAttributes();

            foreach (AnimationAttributes animationAttribute in animationAttributes)
            {
                AnimationClip clip = new AnimationClip();

                foreach (HierarchyAttributes hierarchy in animationAttribute.hierarchy)
                {
                    // prepare single value keyframe lists
                    //
                    List<Keyframe> positionXList = new List<Keyframe>();
                    List<Keyframe> positionYList = new List<Keyframe>();
                    List<Keyframe> positionZList = new List<Keyframe>();

                    List<Keyframe> rotationXList = new List<Keyframe>();
                    List<Keyframe> rotationYList = new List<Keyframe>();
                    List<Keyframe> rotationZList = new List<Keyframe>();
                    List<Keyframe> rotationWList = new List<Keyframe>();

                    List<Keyframe> scaleXList = new List<Keyframe>();
                    List<Keyframe> scaleYList = new List<Keyframe>();
                    List<Keyframe> scaleZList = new List<Keyframe>();

                    foreach (KeyAttributes key in hierarchy.keys)
                    {
                        Keyframe animKey;

                        if (key.usePos == true)
                        {
                            animKey = new Keyframe(key.time, key.pos.x);
                            positionXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.pos.y);
                            positionYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.pos.z);
                            positionZList.Add(animKey);
                        }

                        if (key.useRot == true)
                        {
                            animKey = new Keyframe(key.time, key.rot.x);
                            rotationXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.y);
                            rotationYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.z);
                            rotationZList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.w);
                            rotationWList.Add(animKey);
                        }

                        if (key.useScl == true)
                        {
                            animKey = new Keyframe(key.time, key.scl.x);
                            scaleXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.scl.y);
                            scaleYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.scl.z);
                            scaleZList.Add(animKey);
                        }
                    }

                    // create animation curves
                    //
                    int parent = hierarchy.parent;
                    Transform childTransform = go.transform;
                    if (parent >= 0)
                    {
                        childTransform = boneObjects[parent].transform;
                    }
                    string childPath = GetChildPath(go.transform, childTransform);

                    AnimationCurve curve;

                    if (positionXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = positionXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = positionYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = positionZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.z", curve);
                    }

                    if (rotationXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = rotationXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.z", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationWList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.w", curve);
                    }

                    if (scaleXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = scaleXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = scaleYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = scaleZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.z", curve);
                    }
                }

                clip.EnsureQuaternionContinuity();
                clip.legacy = true;
                clip.wrapMode = WrapMode.Loop;

                string clipFilePath = "Assets/" + objectName + "/Clips/" + animationAttribute.name + ".asset";
                FileSystemHelper.EnsureDirectory(clipFilePath);
                AssetDatabase.CreateAsset(clip, clipFilePath);

                AssetDatabase.Refresh();
                AssetDatabase.ImportAsset(clipFilePath);

                AnimationClip loadedClip = (AnimationClip)AssetDatabase.LoadAssetAtPath(clipFilePath, typeof(UnityEngine.AnimationClip));
                anim.AddClip(loadedClip, animationAttribute.name);
            }

            finishedAction.Invoke(go);
        }

        public static string GetChildPath(Transform rootTransform, Transform childTransform)
        {
            string path = childTransform.name;
            while (childTransform.parent != rootTransform)
            {
                childTransform = childTransform.parent;
                path = childTransform.name + "/" + path;
            }
            return path;
        }

        private void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
        {
            switch (blendMode)
            {
                case BlendMode.Opaque:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt("_ZWrite", 1);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = -1;
                    break;
                case BlendMode.Cutout:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt("_ZWrite", 1);
                    standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 2450;
                    break;
                case BlendMode.Fade:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt("_ZWrite", 0);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 3000;
                    break;
                case BlendMode.Transparent:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt("_ZWrite", 0);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 3000;
                    break;
            }

        }
    }
}