﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CharacterConfigurator
{
    [Serializable]
    public class TextureList
    {
        public List<int> textures;

        public TextureList()
        {
            textures = new List<int>();
        }

        public void Add(int value)
        {
            textures.Add(value);
        }
    }

    [Serializable]
    public class ModelDefinitions
    {
        [SerializeField]
        List<int> _capModels = new List<int>();
        [SerializeField]
        List<int> _hairModels = new List<int>();
        [SerializeField]
        List<TextureList> _capTextures = new List<TextureList>();
        [SerializeField]
        List<TextureList> _hairTextures = new List<TextureList>();
        [SerializeField]
        TextureList _bodyTextures = new TextureList();
        [SerializeField]
        public int _handMaterialIndex;
        [SerializeField]
        public int _sectionIdMaterialIndex;

        [SerializeField]
        int _costumeOffset;

        [SerializeField]
        public int _faceTextureOffset;
        [SerializeField]
        public int _faceTextures;
        [SerializeField]
        public List<int> _faceMapping = new List<int>();
        [SerializeField]
        public int _faceVariations;
        [SerializeField]
        public int _handTextureOffset;
        [SerializeField]
        public int _skinColors;


        [SerializeField]
        public string _class = "";

        [SerializeField]
        public string _modelPrefix = "";

        public static ModelDefinitions CreateFromJSON(string filePath)
        {
            string jsonString = File.ReadAllText(filePath);

            return JsonUtility.FromJson<ModelDefinitions>(jsonString);
        }

        public bool HasBodyTextures()
        {
            if (_bodyTextures.textures.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetBodyTexture(int matIndex)
        {
            if (matIndex >= _bodyTextures.textures.Count)
            {
                Debug.LogWarning("There is no body texture for material index:" + matIndex);

                return 0;
            }

            return _bodyTextures.textures[matIndex];
        }

        public int GetCapTexture(int capIndex, int matIndex)
        {
            if (matIndex >= _capTextures[capIndex].textures.Count)
            {
                Debug.LogWarning("There is no cap texture for material index:" + matIndex);

                return 0;
            }

            return _capTextures[capIndex].textures[matIndex];
        }

        public int GetHairTexture(int hairIndex, int matIndex)
        {
            if (matIndex >= _hairTextures[hairIndex].textures.Count)
            {
                Debug.LogWarning("There is no hair texture for material index:" + matIndex);

                return 0;
            }

            return _hairTextures[hairIndex].textures[matIndex];
        }

        public int GetCapCount()
        {
            return _capModels.Count;
        }

        public int GetHairCount()
        {
            return _hairModels.Count;
        }

        public int GetCostumeOffset()
        {
            return _costumeOffset;
        }

        public bool HasCapModels()
        {
            if (_capModels.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetCapModelIndex(int variation)
        {
            return _capModels[variation];
        }

        public bool HasHairModels()
        {
            if (_hairModels.Count > 0)
            {
                return true;
            }

            return false;
        }

        public int GetHairModelIndex(int variation)
        {
            return _hairModels[variation];
        }
    }

    ModelDefinitions _modelDefinitions;

    GameObject _body;
    GameObject _hair;
    GameObject _cap;
    GameObject _head;

    public int _costume = 0;
    public int _capVariation = 0;
    public int _hairVariation = 0;
    public int _faceVariation = 0;
    public int _skinColor = 0;

    public Color _hairColor = new Color(1.0f, 1.0f, 1.0f);

    public string _class;

    public void Init(string characterClass)
    {
        _class = characterClass;

        string path = Application.streamingAssetsPath + "/" + _class + ".json";

        _modelDefinitions = ModelDefinitions.CreateFromJSON(path);
    }

    public void SetModelLinks(GameObject body, GameObject hair, GameObject cap, GameObject head)
    {
        _body = body;
        _hair = hair;
        _cap = cap;
        _head = head;
    }

    public void ApplyConfiguration()
    {
        if (_modelDefinitions.HasBodyTextures() == true)
        {
            // apply body textures
            //
            Renderer bodyRenderer = _body.GetComponent<Renderer>();
            for (int materialIndex = 0; materialIndex < bodyRenderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetBodyTexture(materialIndex);

                if (materialIndex == _modelDefinitions._handMaterialIndex)
                {
                    textureIndex = _modelDefinitions._handTextureOffset + _skinColor;
                }
                else
                {
                    textureIndex = GetCostumeOffset(textureIndex);
                }

                string texturePath = GetTexturePath(textureIndex);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));

                bodyRenderer.sharedMaterials[materialIndex].mainTexture = texture;
            }
        }

        if (_modelDefinitions.HasCapModels() == true)
        {
            // apply cap textures
            Renderer renderer = _cap.GetComponentInChildren<Renderer>();
            for (int materialIndex = 0; materialIndex < renderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetCapTexture(_capVariation, materialIndex);
                textureIndex = GetCostumeOffset(textureIndex);
                string texturePath = GetTexturePath(textureIndex);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));
                renderer.sharedMaterials[materialIndex].mainTexture = texture;
            }
        }

        if (_modelDefinitions.HasHairModels() == true)
        {
            // apply hair textures and color
            Renderer renderer = _hair.GetComponentInChildren<Renderer>();
            for (int materialIndex = 0; materialIndex < renderer.sharedMaterials.Length; materialIndex++)
            {
                int textureIndex = _modelDefinitions.GetHairTexture(_hairVariation, materialIndex);
                textureIndex = GetCostumeOffset(textureIndex);
                string texturePath = GetTexturePath(textureIndex);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));
                renderer.sharedMaterials[materialIndex].mainTexture = texture;
                renderer.sharedMaterials[materialIndex].SetColor("_Color", _hairColor);
            }
        }

        // apply face texture
        //
        {
            Renderer faceRenderer = _head.GetComponent<Renderer>();

            for (int materialIndex = 0; materialIndex < _modelDefinitions._faceMapping.Count; materialIndex++)
            {
                int textureIndex = _modelDefinitions._faceTextureOffset +
                                    (_faceVariation * _modelDefinitions._faceTextures * _modelDefinitions._skinColors) +
                                    _skinColor;

                textureIndex += _modelDefinitions._faceMapping[materialIndex];

                string texturePath = GetTexturePath(textureIndex);
                Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture));

                faceRenderer.sharedMaterials[materialIndex].mainTexture = texture;
            }
        }
    }

    private string GetTexturePath(int textureIndex)
    {
        string texturePath = "Assets/" + _modelDefinitions._class + "/textures/" +
                                                  _modelDefinitions._modelPrefix + "tex_" + string.Format("{0:000}", textureIndex) + ".png";
        return texturePath;
    }

    private int GetCostumeOffset(int textureIndex)
    {
        int costume = (_costume % 9);

        if (textureIndex >= 10000)
        {
            int customCostumeOffset = textureIndex / 10000;
            textureIndex = textureIndex % 10000;
            textureIndex += customCostumeOffset * costume;
        }
        else if (textureIndex < _modelDefinitions.GetCostumeOffset())
        {
            int costumeOffset = costume * _modelDefinitions.GetCostumeOffset();
            textureIndex += costumeOffset;
        }

        return textureIndex;
    }

    public string GetRootPath()
    {
        string path = Application.streamingAssetsPath + "/ply_" + _class + "/";

        return path;
    }

    public string GetModelPath()
    {
        string path = GetRootPath() + "dmf/";

        return path;
    }

    public string GetTexturePath()
    {
        string path = GetRootPath() + "png/";

        return path;
    }

    public string GetBodyPath()
    {
        return GetModelPath() + _modelDefinitions._modelPrefix + "bdy00.dmf";
    }

    public string GetHeadPath()
    {
        return GetModelPath() + _modelDefinitions._modelPrefix + "hed00.dmf";
    }

    public int GetCapCount()
    {
        return _modelDefinitions.GetCapCount();
    }

    public string GetCapPath(int index)
    {
        return GetModelPath() + _modelDefinitions._modelPrefix + "cap" + index.ToString("00") + ".dmf";
    }

    public int GetHairCount()
    {
        return _modelDefinitions.GetHairCount();
    }

    public string GetHairPath(int index)
    {
        return GetModelPath() + _modelDefinitions._modelPrefix + "hai" + index.ToString("00") + ".dmf";
    }

    public int GetFaceVariations()
    {
        return _modelDefinitions._faceVariations;
    }

    public int GetSkinColors()
    {
        return _modelDefinitions._skinColors;
    }
}
