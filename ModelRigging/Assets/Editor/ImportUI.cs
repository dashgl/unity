﻿/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

using UnityEngine;

using UnityEditor;
using System.Collections.Generic;

using DashImporter;
using System.IO;

public class ImportUI : EditorWindow
{
    public static string _modelFile = "";

    private Importer _importer;
    private CharacterConfigurator _configurator = null;


    public static string _class = "";
    public static Transform _body = null;
    public static Transform _cap = null;
    public static Transform _hair = null;
    public static Transform _tposeRoot = null;

    private List<string> _fileList;
    private int _fileIndex;

    private List<GameObject> _objectList;

    private int _costume = 1;
    private int _capVariation = 0;
    private int _faceVariation = 0;
    private int _skinColor = 0;
    private int _hairVariation = 0;
    private Color _hairColor = new Color(1.0f, 0.7f, 0.3f);

    [MenuItem("Dash/Import Model")]

    static void Init()
    {
        UnityEditor.EditorWindow window = GetWindow(typeof(ImportUI));
        window.position = new Rect(50, 50, 500, 200);
        window.Show();
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void OnGUI()
    {
        // source path
        //
        EditorGUIUtility.labelWidth = 80;
        _class = EditorGUILayout.TextField("Class: ", _class);

        if (GUILayout.Button("Import Model"))
        {
            _configurator = new CharacterConfigurator();
            _configurator.Init(_class);

            _importer = new Importer();

            // prepare model file list
            //
            _fileList = new List<string>();
            _objectList = new List<GameObject>();

            _fileList.Add(_configurator.GetBodyPath());
            _fileList.Add(_configurator.GetHeadPath());

            for (int index = 0; index < _configurator.GetCapCount(); index++)
            {
                _fileList.Add(_configurator.GetCapPath(index));
            }

            for (int index = 0; index < _configurator.GetHairCount(); index++)
            {
                _fileList.Add(_configurator.GetHairPath(index));
            }

            // import model files
            //
            StartFileImport();

            // prepare model hierarchy
            //

            // attach head to body
            GameObject bodyObject = _objectList[0];
            GameObject headObject = _objectList[1];
            GameObject headBone = FindChild(_objectList[0], "bone_059");

            if (headObject != null)
            {
                FixBoneNames(headObject.name, headObject);

                headObject.transform.SetParent(headBone.transform, false);
                headObject.transform.localScale = Vector3.one;
            }

            // attach caps to body
            for (int index = 0; index < _configurator.GetCapCount(); index++)
            {
                GameObject capObject = _objectList[2 + index];

                FixBoneNames(capObject.name, capObject);

                capObject.transform.SetParent(headBone.transform, false);
                capObject.transform.localScale = Vector3.one;
            }

            // attach hair to body
            for (int index = 0; index < _configurator.GetHairCount(); index++)
            {
                GameObject hairObject = _objectList[2 + _configurator.GetCapCount() + index];

                FixBoneNames(hairObject.name, hairObject);

                hairObject.transform.SetParent(headBone.transform, false);
                hairObject.transform.localScale = Vector3.one;
            }

            // t-pose
            // bone_027 localEuler: x: -90 y: 90 z 0
            // bone_040 localEuler: x: 90 y: 90 z 0

            // prepare t-pose
            GameObject bone = FindChild(bodyObject, "bone_027");
            bone.transform.localEulerAngles = new Vector3(-90f, 90f, 0f);
            bone = FindChild(bodyObject, "bone_040");
            bone.transform.localEulerAngles = new Vector3(90f, 90f, 0f);

            // re-position Root to spine
            GameObject spineReference = FindChild(bodyObject, "bone_001");
            bone = FindChild(bodyObject, "bone_000");
            Vector3 pos = spineReference.transform.position;
            bodyObject.transform.position = pos;
            bone.transform.position = pos;
            spineReference.transform.position = pos;

            // set root to 0,1,0
            Vector3 yOnePos = new Vector3(0f, 1f, 0f);
            pos = bone.transform.position;      // save first child
            bodyObject.transform.position = yOnePos;
            bone.transform.position = pos;      // restor first child

            // rename root
            bodyObject.name = "Root";
            bodyObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);    // fix root scale

            // import model textures
            ImportTextures();

            AssignCharacterConfiguration(); // default configuration
        }

        GUILayout.Space(8);
        if (_configurator != null)
        {
            _costume = EditorGUILayout.IntSlider("Costume", _costume, 0, 10);
            _capVariation = EditorGUILayout.IntSlider("Cap", _capVariation, 0, _configurator.GetCapCount() - 1);
            _faceVariation = EditorGUILayout.IntSlider("Face", _faceVariation, 0, _configurator.GetFaceVariations() - 1);
            _skinColor = EditorGUILayout.IntSlider("Skin", _skinColor, 0, _configurator.GetSkinColors() - 1);
            _hairColor = EditorGUILayout.ColorField("Hair Color", _hairColor);

            if (GUILayout.Button("Assign Configuration"))
            {
                AssignCharacterConfiguration();
            }
        }

        if (GUILayout.Button("Build Avatar"))
        {
            GameObject bodyObject = _objectList[0];

            BuildAvatar.CreateCharacter(bodyObject.transform, "skeleton", _class);
        }
    }

    private void StartFileImport()
    {
        _fileIndex = 0;

        ImportNextFile();
    }

    private void ImportNextFile()
    {
        if (_fileIndex < _fileList.Count)
        {
            _importer.Import(_fileList[_fileIndex], OnLoadingFinsihed, _class + "/");
        }
    }

    private void OnLoadingFinsihed(GameObject obj)
    {
        _objectList.Add(obj);

        _fileIndex++;

        ImportNextFile();
    }

    private void AssignCharacterConfiguration()
    {
        HideCapVariations();

        GameObject capObject = _objectList[2 + _capVariation];
        capObject.SetActive(true);
        GameObject hairObject = _objectList[2 + _configurator.GetCapCount() + _capVariation];
        hairObject.SetActive(true);

        _configurator.SetModelLinks(_objectList[0], hairObject, capObject, _objectList[1]);

        _configurator._costume = _costume;

        _configurator._capVariation = _capVariation;
        _configurator._hairVariation = _capVariation;

        _configurator._faceVariation = _faceVariation;
        _configurator._skinColor = _skinColor;

        _configurator._hairColor = _hairColor;

        _configurator.ApplyConfiguration();
    }

    private void HideCapVariations()
    {
        for (int index = 0; index < _configurator.GetCapCount(); index++)
        {
            GameObject capObject = _objectList[2 + index];
            capObject.SetActive(false);
            GameObject hairObject = _objectList[2 + _configurator.GetCapCount() + index];
            hairObject.SetActive(false);
        }
    }

    public GameObject FindChild(GameObject parent, string childName)
    {
        Transform[] childTransforms = parent.GetComponentsInChildren<Transform>();

        foreach (Transform child in childTransforms)
        {
            if (child.name == childName)
            {
                return child.gameObject;
            }
        }

        return null;
    }

    public void FixBoneNames(string prefix, GameObject obj)
    {
        Transform[] childTransforms = obj.GetComponentsInChildren<Transform>();

        foreach (Transform child in childTransforms)
        {
            if (child.name.StartsWith("bone"))
            {
                child.name = prefix + child.name;
            }
        }
    }

    public void ImportTextures()
    {
        string[] fileList = FileSystemHelper.GetFileListFromFolder(_configurator.GetTexturePath(), "png");

        foreach (string file in fileList)
        {
            ImportTexture(file, "Assets/" + _class + "/Textures/");
        }

        AssetDatabase.Refresh();
    }

    public void ImportTexture(string sourceFile, string destPath)
    {
        if (Directory.Exists(destPath) == false)
        {
            Directory.CreateDirectory(destPath);
        }

        if (File.Exists(sourceFile) == false)
        {
            return;
        }

        FileInfo fileInfo = new FileInfo(sourceFile);
        if (fileInfo.Extension != ".png")
        {
            return;
        }

        string file = FileSystemHelper.GetFileNameFromPath(sourceFile);

        string filePath = destPath + "/" + file;

        if (File.Exists(filePath) == false)
        {
            File.Copy(sourceFile, filePath, true);
        }

        AssetDatabase.ImportAsset(filePath);
    }
}

