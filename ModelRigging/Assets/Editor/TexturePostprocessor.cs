﻿using UnityEngine;
using UnityEditor;

class TexturePostprocessor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        TextureImporter textureImporter = (TextureImporter)assetImporter;
        textureImporter.wrapMode = TextureWrapMode.Mirror;
    }
}
