﻿
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class BuildAvatar
{
    public static GameObject CreateCharacter(Transform root, string skeletonfile = "", string path = "")
    {
        GameObject character = root.gameObject;

        // set up the description for the humanoid, given parameters
        HumanDescription desc = SetupHumanDescription(character, skeletonfile);

        // create animator if needed
        if (character.GetComponent<Animator>() == null)
        {
            character.AddComponent<Animator>();
        }

        // create the avatar using that gameobject and the human description we created
        Avatar avatar = AvatarBuilder.BuildHumanAvatar(character, desc);
        if (avatar == null)
        {
            Debug.LogError("Could not build Avatar.");

            return null;
        }

        character.GetComponent<Animator>().avatar = avatar;

        // create avatar asset
        string objectName = "Avatar";
        string avatarFilePath = "Assets/" + path + "/" + objectName + ".asset";
        AssetDatabase.CreateAsset(avatar, avatarFilePath);

        return character;
    }

    public static HumanDescription SetupHumanDescription(GameObject character, string skeletonfile = "")
    {
        HumanDescription desc = new HumanDescription();

        // store tpose (current pose)
        //
        Dictionary<string, Transform> tPoseDict;
        tPoseDict = ExportTPoseToDictionary(character.transform);

        // first load in the bone mapping from the file, if that file is provided
        List<HumanBone> mappedBones = new List<HumanBone>();
        List<SkeletonBone> allBones = new List<SkeletonBone>();
        if (skeletonfile != "")
        {
            LoadSkeletonCorrespondences(character, skeletonfile, ref mappedBones, ref allBones);
        }

        // Set up the parameters for the human description
        //
        HumanBone[] human = mappedBones.ToArray();

        foreach (KeyValuePair<string, Transform> pair in tPoseDict)
        {
            SkeletonBone skBone = new SkeletonBone();

            Transform transform = pair.Value;

            skBone.name = transform.name;

            skBone.rotation = new Quaternion(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);

            skBone.position = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
            skBone.scale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);

            allBones.Add(skBone);
        }

        // set the bone arrays right
        desc.human = human;
        desc.skeleton = allBones.ToArray();

        // set the default values for the rest of the human descriptor parameters
        desc.upperArmTwist = -0.5f;
        desc.lowerArmTwist = 0.5f;
        desc.upperLegTwist = 0.5f;
        desc.lowerLegTwist = 0.5f;
        desc.armStretch = 0.05f;
        desc.legStretch = 0.05f;
        desc.feetSpacing = 0.0f;

        // return the human description
        return desc;
    }

    public static void LoadSkeletonCorrespondences(GameObject character, string skeletonFile, ref List<HumanBone> mappedBones, ref List<SkeletonBone> allBones)
    {
        // load in the skeleton definition file from an asset
        TextAsset skel = (TextAsset)Resources.Load(skeletonFile);

        if (skel == null)
        {
            Debug.LogError("Cant load " + skeletonFile);
            return;
        }

        string[] lines = skel.text.Split(new char[] { '\n' });

        for (int i = 0; i < lines.Length; i++)
        {
            //if it doesn't have a comma, skip it
            if (lines[i].IndexOf(',') != -1)
            {
                // split the line by comma
                string[] bones = lines[i].Split(new char[] { ',' });

                //if the bone has a mapping, add it to the mappedBones list as a HumanBone
                if (bones[0].Trim() != "none")
                {
                    HumanBone b = new HumanBone();
                    b.boneName = bones[1].Trim();
                    b.humanName = bones[0].Trim();

                    // set the bone limit to use default values
                    b.limit.useDefaultValues = true;

                    mappedBones.Add(b);
                }
            }
        }
    }

    public static Dictionary<string, Transform> ExportTPoseToDictionary(Transform root)
    {
        Transform xform = root;
        Dictionary<string, Transform> xforms = new Dictionary<string, Transform>();

        // recursively visit the children, gathering their transform data into the xforms dictionary
        RecursiveTransform(xform, ref xforms);

        return xforms;
    }

    public static void RecursiveTransform(Transform current, ref Dictionary<string, Transform> xforms)
    {
        xforms.Add(current.name, current);
        for (int i = 0; i < current.childCount; ++i)
        {
            RecursiveTransform(current.GetChild(i), ref xforms);
        }
    }

    public static Transform RecursiveSearch(string name, Transform current)
    {
        if (current.name == name)
        {
            return current;
        }
        else
        {
            for (int i = 0; i < current.childCount; ++i)
            {
                Transform found = RecursiveSearch(name, current.GetChild(i));

                if (found != null)
                {
                    return found;
                }
            }
        }

        return null;
    }
}
