﻿/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

using UnityEngine;
using UnityEngine.Networking;

using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using System;
using System.IO;
using System.Text.RegularExpressions;
using DashImporter;

public class ImportUI : EditorWindow
{
    public static string _modelFile = "";

    private Importer _importer;

    public static string _class = "";
    public static Transform _body = null;
    public static Transform _cap = null;
    public static Transform _hair = null;

    [MenuItem("Dash/Import Model")]

    static void Init()
    {
        UnityEditor.EditorWindow window = GetWindow(typeof(ImportUI));
        window.position = new Rect(50, 50, 500, 200);
        window.Show();
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void OnGUI()
    {
        // source path
        //
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 80;
        _modelFile = EditorGUILayout.TextField("Model path: ", _modelFile);
        if (GUILayout.Button("...", GUILayout.Width(32)))
        {
            _modelFile = EditorUtility.OpenFilePanel("Select source data path", "", "");
            this.Repaint();
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Import Model"))
        {
            _importer = new Importer();

            _importer.Import(_modelFile, OnLoadingFinsihed);
        }
/*
        GUILayout.Space(8);
        _class = EditorGUILayout.TextField("Class: ", _class);
        _body = EditorGUILayout.ObjectField("Body", _body, typeof(Transform), true) as Transform;
        _cap = EditorGUILayout.ObjectField("Cap", _cap, typeof(Transform), true) as Transform;
        _hair = EditorGUILayout.ObjectField("Hair", _hair, typeof(Transform), true) as Transform;

        if (GUILayout.Button("Assign Configuration"))
        {
            AssignCharacterConfiguration();
        }
*/
    }

    private void OnLoadingFinsihed(GameObject obj)
    {
        //        Debug.Log("loading finished");
/*
        loading = false;

        _loadedObject = obj;

        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.enabled = false;
        }
*/
        Debug.Log("Done loading: ");
    }

    private void AssignCharacterConfiguration()
    {
        CharacterConfigurator configurator = new CharacterConfigurator();

        configurator.Init(_class);
        configurator.SetModelLinks(_body.gameObject, _hair.gameObject, _cap.gameObject);
        configurator.ApplyConfiguration();
    }

}

