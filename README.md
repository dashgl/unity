# DashUnityProject

A Repository for Unity projects that handle the dash model format

- RuntimeLoader: 
  Project which includes the required files to create a basic scene and load a .dmf file as a streaming asset in runtime environment.
  
- AssetImporter:
  Project that contains the necessary files to import .dmf files into an unity editor environment. Making it possible to modify the assets and export them as .unitypackage.